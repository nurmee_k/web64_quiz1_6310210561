import ForecastResult from "../components/ForecastResult";
import { useState } from "react";

function ForecastCalPage () {

    const [ name, setName ] = useState("");
    const [ ForecastResult, setForecastResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");
   
    const [ year, setYear ] = useState("");
    const [ age, setAge ] = useState("");

    function CalculateForecast() {
        let y = parseInt(year);
        let a = parseInt(age);
        let forecast = (y * a) / 100;
        setForecastResult( forecast );
        if (forecast > 250){
            setTranslateResult("โชคดี")
        }else {
            setTranslateResult("โชคร้าย")
        }

    }

    return (
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บพยากรณ์ดวง
                <hr />

                คุณชื่อ: <input type="text" 
                            value={name}
                            onChange={ (e) => { setName(e.target.value); } } /> <br /> 
                อายุ: <input type="text" 
                            value={age}
                            onChange={ (e) => { setAge(e.target.value); } } /> <br />
                ปีเกิด: <input type="text" 
                            value={year}
                            onChange={ (e) => { setYear(e.target.value); } } /> <br />

                <button onClick={ ()=>{ CalculateForecast() } }> Calculate </button>
                {   ForecastResult != 0 && 
                        <div>
                            <hr />
                            ผลการพยากรณ์

                            <ForecastResult 
                                name= { name }
                                forecast ={ ForecastResult }
                                result ={ translateResult}

                            />
                        </div>
                }
            </div>

        </div>


    );
}

export default ForecastCalPage; 

import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import ForecastCalPage from './pages/ForecastCalPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
        <Header /> 
        <Routes>
            <Route path="about" element={
                      <AboutUsPage />
                        } />
           
            <Route path="/" element={
                      <ForecastCalPage />
                    } />

        </Routes>  
    </div>
  );
}

export default App;

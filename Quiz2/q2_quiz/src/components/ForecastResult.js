function ForecastResult (props) {


    return (
        <div>
            <h3>คุณ: {props.name}</h3>
            <h3>อายุ: {props.age}</h3>
            <h3>ปีเกิด: {props.year}</h3>
            <h3>แปลว่า: {props.result}</h3>

        </div>

    );
}

export default ForecastResult;


function AboutUs (props) {

    return (
        <div>
            <h2> จัดทำโดย: { props.name } </h2>
            <h3> รหัสนักศึกษา { props.studentid } </h3>
        </div>

    );
}

export default AboutUs;